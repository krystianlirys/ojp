#pragma once
#include"Database.h"
#include"stdafx.h"
#include"Form1.h"
#include"list"

namespace Database{
	Column::Column(WindowsFormApplication1::Form1 ^mainFrame):
		numberOfColumn(0),
		x(10),
		width(80),
		tabIndex(3)
	{
		columnName = gcnew List<System::Windows::Forms::TextBox^>;
		this->mainFrame=mainFrame;
		Database::Column::push_back();
	}
	void Column::push_back(){
		columnName->Add(gcnew System::Windows::Forms::TextBox()); 
		columnName[numberOfColumn]->Location = System::Drawing::Point(x, 111);
		columnName[numberOfColumn]->Name = L"columnName"+(numberOfColumn);
		columnName[numberOfColumn]->Size = System::Drawing::Size(width, 20);
		columnName[numberOfColumn]->TabIndex = 3;
		columnName[numberOfColumn]->Text = L"Column";
		columnName[numberOfColumn]->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		columnName[numberOfColumn]->WordWrap = false;
		columnName[numberOfColumn]->TextChanged += gcnew System::EventHandler(mainFrame, &WindowsFormApplication1::Form1::columnName_TextChanged);
		mainFrame->Controls->Add(this->columnName[numberOfColumn]);
		x+=width+10;
		numberOfColumn++;
		tabIndex++;
	}
	void Column::click(){
		columnName[numberOfColumn]->Text = "";
		columnName[numberOfColumn]->Select();
		push_back();
	}
}