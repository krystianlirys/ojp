// WindowsFormApplication1.cpp : main project file.

#include "stdafx.h"
#include "Form1.h"
#include "Database.h"

using namespace WindowsFormApplication1;

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{
	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false); 

	// Create the main window and run it
	Form1 ^mainFrame=gcnew Form1();
	Database::Column column(mainFrame);
	Application::Run(mainFrame);
	return 0;
}
