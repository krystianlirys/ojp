#pragma once
#include<cstdlib>
#include<cmath>
#include<list>
#include"Database.h"

namespace WindowsFormApplication1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  databaseToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  newDatabaseToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  importDatabaseToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  searchItemToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  addNewItemToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  deleteItemToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::TextBox^  databaseName;

	private: System::Windows::Forms::Label^  label1;
	//private: System::Windows::Forms::TextBox^  columnName;
	private: Database::Column^ columnName;






	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}


	protected: 

	protected: 




































	private: System::ComponentModel::IContainer^  components;



	protected:

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->databaseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->newDatabaseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->importDatabaseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->searchItemToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->addNewItemToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->deleteItemToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->helpToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->databaseName = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());

			this->columnName = gcnew Database::Column(this);
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->databaseToolStripMenuItem, 
				this->helpToolStripMenuItem, this->helpToolStripMenuItem1});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(418, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// databaseToolStripMenuItem
			// 
			this->databaseToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->newDatabaseToolStripMenuItem, 
				this->importDatabaseToolStripMenuItem});
			this->databaseToolStripMenuItem->Name = L"databaseToolStripMenuItem";
			this->databaseToolStripMenuItem->Size = System::Drawing::Size(67, 20);
			this->databaseToolStripMenuItem->Text = L"Database";
			// 
			// newDatabaseToolStripMenuItem
			// 
			this->newDatabaseToolStripMenuItem->Name = L"newDatabaseToolStripMenuItem";
			this->newDatabaseToolStripMenuItem->Size = System::Drawing::Size(160, 22);
			this->newDatabaseToolStripMenuItem->Text = L"New database";
			// 
			// importDatabaseToolStripMenuItem
			// 
			this->importDatabaseToolStripMenuItem->Name = L"importDatabaseToolStripMenuItem";
			this->importDatabaseToolStripMenuItem->Size = System::Drawing::Size(160, 22);
			this->importDatabaseToolStripMenuItem->Text = L"Import database";
			// 
			// helpToolStripMenuItem
			// 
			this->helpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->searchItemToolStripMenuItem, 
				this->addNewItemToolStripMenuItem, this->deleteItemToolStripMenuItem});
			this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
			this->helpToolStripMenuItem->Size = System::Drawing::Size(43, 20);
			this->helpToolStripMenuItem->Text = L"Item";
			// 
			// searchItemToolStripMenuItem
			// 
			this->searchItemToolStripMenuItem->Name = L"searchItemToolStripMenuItem";
			this->searchItemToolStripMenuItem->Size = System::Drawing::Size(136, 22);
			this->searchItemToolStripMenuItem->Text = L"Search item";
			// 
			// addNewItemToolStripMenuItem
			// 
			this->addNewItemToolStripMenuItem->Name = L"addNewItemToolStripMenuItem";
			this->addNewItemToolStripMenuItem->Size = System::Drawing::Size(136, 22);
			this->addNewItemToolStripMenuItem->Text = L"Add item";
			// 
			// deleteItemToolStripMenuItem
			// 
			this->deleteItemToolStripMenuItem->Name = L"deleteItemToolStripMenuItem";
			this->deleteItemToolStripMenuItem->Size = System::Drawing::Size(136, 22);
			this->deleteItemToolStripMenuItem->Text = L"Delete item";
			// 
			// helpToolStripMenuItem1
			// 
			this->helpToolStripMenuItem1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->aboutToolStripMenuItem});
			this->helpToolStripMenuItem1->Name = L"helpToolStripMenuItem1";
			this->helpToolStripMenuItem1->Size = System::Drawing::Size(44, 20);
			this->helpToolStripMenuItem1->Text = L"Help";
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(107, 22);
			this->aboutToolStripMenuItem->Text = L"About";
			// 
			// databaseName
			// 
			this->databaseName->Location = System::Drawing::Point(110, 51);
			this->databaseName->Name = L"databaseName";
			this->databaseName->Size = System::Drawing::Size(189, 20);
			this->databaseName->TabIndex = 1;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(22, 58);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(82, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Database name";
			this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// columnName
			// 
			/*this->columnName->Location = System::Drawing::Point(12, 111);
			this->columnName->Name = L"columnName";
			this->columnName->Size = System::Drawing::Size(84, 20);
			this->columnName->TabIndex = 3;
			this->columnName->Text = L"Column";
			this->columnName->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->columnName->WordWrap = false;
			this->columnName->TextChanged += gcnew System::EventHandler(this, &Form1::columnName_TextChanged);*/
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(418, 356);
			//this->Controls->Add(this->columnName);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->databaseName);
			this->Controls->Add(this->menuStrip1);
			this->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->KeyPreview = true;
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"Kalk 2000";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

public: System::Void columnName_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 
		 }
};
}

